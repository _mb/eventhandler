package services

import javax.inject._

import scala.collection.JavaConverters._

trait ReportMaker {

  def calcDifferentValuesReport(): List[EventStatistic]

}

@Singleton
class LocalReportMaker @Inject()(val eventHolder: EventHolder) extends ReportMaker {
  override def calcDifferentValuesReport(): List[EventStatistic] = {
    eventHolder.getEvents()
      .foldLeft(Map[String, Set[Long]]())((resMap, ev) => resMap + ((ev.name, resMap.getOrElse(ev.name, Set[Long]()) + ev.value)))
      .map(e => EventStatistic(e._1, e._2.size)).toList;
  }
}
