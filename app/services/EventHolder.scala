package services

import javax.inject._

import scala.collection.JavaConverters._
import  scala.collection._
import java.util.concurrent.ConcurrentHashMap

trait EventHolder {
  def handleEvent(event: Event): Unit

  def getEvents() : List[Event]
}

@Singleton
class ConcurrentEventHolder extends EventHolder {

  private val eventMap: concurrent.Map[String, Set[Long]] = new ConcurrentHashMap[String, Set[Long]]().asScala;

  override def handleEvent(event: Event): Unit = {
    val eventValues = eventMap.getOrElse(event.name.intern, Set[Long]());
    eventMap += ((event.name, eventValues + event.value));
  }

  override def getEvents(): List[Event] = {
    eventMap.flatMap(e => e._2.map(v => Event(e._1, v))).toList
  }
}
