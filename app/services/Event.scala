package services

case class Event(name: String, value: Long)

case class EventStatistic(name: String, metric: Int)
