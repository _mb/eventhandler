package controllers

import javax.inject._
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc._
import services.{EventStatistic, ReportMaker}

@Singleton
class ReportController @Inject()(cc: ControllerComponents,
                                 statistics: ReportMaker) extends AbstractController(cc) {

  def report: Action[AnyContent] = Action {
    val json = Json.toJson(statistics.calcDifferentValuesReport())
    Ok(json)
  }

  implicit val statisticsWrites: Writes[EventStatistic] = {
    ((JsPath \ "event_name").write[String] and
      (JsPath \ "value").write[Int]) (unlift(EventStatistic.unapply))
  }

}
