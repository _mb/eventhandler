package controllers

import javax.inject._
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._
import play.api.mvc._
import services.{Event, EventHolder}

@Singleton
class EventController @Inject()(cc: ControllerComponents,
                                eventHolder: EventHolder) extends AbstractController(cc) {

  def handleEvent: Action[JsValue] = Action(parse.json) { request =>
    val placeResult: JsResult[Event] = request.body.validate[Event]
    placeResult.fold(
      errors => {
        BadRequest(Json.obj("errors" -> JsError.toJson(errors)))
      },
      event => {
        eventHolder.handleEvent(event)
        Ok("")
      }
    )
  }

  implicit val eventReads: Reads[Event] = {
    ((JsPath \ "event_name").read[String] and
      (JsPath \ "value").read[Long]).apply(Event.apply _)
  }

}
